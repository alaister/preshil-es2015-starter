# Simple Webpack ES2015 Starter Pack

To start your app:

  * Install dependencies with `npm install`
  * Start server and compile JS with `npm run dev`

Now you can visit [`localhost:3000`](http://localhost:3000) from your browser.