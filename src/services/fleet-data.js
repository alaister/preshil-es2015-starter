​export let fleet = [
  {
    license: "ABC123",
    type: "drone",
    model: "Amazon 1550",
    airTimeHours: "2100",
    base: "Melbourne",
    latLong: "37.813611, 144.963111"
  },
  {
    license: "XYZ456",
    type: "drone",
    model: "Google 3800",
    airTimeHours: "300",
    base: "New York",
    latLong: "40.779423 -73.969411"
  },
  {
    license: "AT9900",
    type: "car",
    make: "Tesla",
    model: "Quick Transport",
    miles: "15600",
    latLong: "40.773272 -73.968875"
  }
]