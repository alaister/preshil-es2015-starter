// Make sure you import everything you want to happen in this file to be executed.
import Car from "./classes/car"
import Drone from "./classes/drone"

const myCar = new Car()
const myDrone = new Drone()
